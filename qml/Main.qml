/*
 * Copyright (C) 2020  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * searchlight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
//import QtSensors 5.0

import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'searchlight.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property bool       isLandscape: width > height
    property bool     isGameRunning: true
    property bool       rightCopOut: false
    property bool        leftCopOut: false
    property bool           isGameB: false
    property bool     playerDelayed: false
    property int           wallHole: 0
    property int              score: 0
    property int    lightSpeedTimer: 550
    property int  copLeftSpeedTimer: 1250
    property int copRightSpeedTimer: 1150
    property int      levelUpFactor: 3

    property bool         debugging: false
    property int        buttonWidth: units.gu(15)
    property int buttonBottomMargin: units.gu(6)
    property int buttonSideMargin: root.width > units.gu(36)
        ? units.gu(3)
        : units.gu(1)

    backgroundColor: "#cdcdcd"

    onScoreChanged: if (score % 150 == 0) {
        levelUp()
    }

/*
    Accelerometer {
        id: accel
        active: true

        property real smoothing: 0.03
        property real ay

        onReadingChanged: {
            ay = smoothing * reading.y + (1 - smoothing) * ay
        }
    }
*/
    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            visible: !isLandscape
            title: i18n.tr('Search Light Handheld')
        }

        Item {
            id: lcd
            width: isLandscape ? (parent.height - header.height * 2) * 4/3 : parent.width

            anchors {
                top: header.visible ? header.bottom : parent.top
                horizontalCenter: parent.horizontalCenter
                topMargin: isLandscape ? header.height : 0
            }

            GameElement {
                width: parent.width
                image: Qt.resolvedUrl("../assets/lcd/base.svg")
            }

            GameElement {
                width: parent.width
                image: Qt.resolvedUrl("../assets/lcd/base-all-off.svg")
                //opacity: ((accel.ay +2)*.1) % 10
            }

            Sprite {
                id: thousands
                width: parent.width
                anchors.top: parent.top

                property var numbers: [
                    "number-0.svg",
                    "number-1.svg",
                    "number-2.svg",
                    "number-3.svg",
                    "number-4.svg",
                    "number-5.svg",
                    "number-6.svg",
                    "number-7.svg",
                    "number-8.svg",
                    "number-9.svg"
                ]

                Component.onCompleted: {
                    orderedImages = thousands.numbers
                    updateImage(-1)
                }

                Connections {
                    target: root
                    onScoreChanged: thousands.showImage(Math.floor(score / 1000 % 10))
                }
            }

            Sprite {
                id: hundrends
                width: parent.width
                anchors {
                    top: parent.top
                    left: parent.left
                    leftMargin: parent.width * .047
                }

                Component.onCompleted: {
                    orderedImages = thousands.numbers
                    updateImage(-1)
                }

                Connections {
                    target: root
                    onScoreChanged: hundrends.showImage(Math.floor(score / 100 % 10))
                }
            }

            Sprite {
                id: tens
                width: parent.width
                anchors {
                    top: parent.top
                    left: parent.left
                    leftMargin: parent.width * .047 *2
                }

                Component.onCompleted: {
                    orderedImages = thousands.numbers
                    updateImage(-1)
                }

                Connections {
                    target: root
                    onScoreChanged: tens.showImage(Math.floor(score / 10 % 10))
                }
            }

            Sprite {
                id: ones
                width: parent.width
                anchors {
                    top: parent.top
                    left: parent.left
                    leftMargin: parent.width * .047 * 3
                }

                Component.onCompleted: {
                    orderedImages = thousands.numbers
                    updateImage(-1)
                }

                Connections {
                    target: root
                    onScoreChanged: ones.showImage(Math.floor(score % 10))
                }
            }

            Sprite {
                id: lightsOn
                width: parent.width
                anchors.top: parent.top

                orderedImages:[
                    "light-on-00.svg",
                    "light-on-01.svg",
                    "light-on-02.svg",
                    "light-on-03.svg",
                    "light-on-04.svg",
                    "light-on-05.svg",
                    "light-on-04.svg",
                    "light-on-03.svg",
                    "light-on-02.svg",
                    "light-on-01.svg"
                ]

                Component.onCompleted: updateImage(-1)

                onCheckCollision: playerCaughtByLight()
            }

            Sprite {
                id: leftCop
                width: parent.width
                anchors.top: parent.top

                property var regularLeftCop: [
                    "",
                    "cop-a-00.svg",
                    "cop-a-01.svg",
                    "cop-a-02.svg",
                    "cop-a-03.svg",
                    "cop-a-03.svg",
                    "cop-a-02.svg",
                    "cop-a-01.svg",
                    "cop-a-00.svg"
                ]

                property var leftCopDog: [
                    "cop-a-04.svg"
                ]

                onCurrentImageChanged: checkLeftCop()

                Component.onCompleted: {
                    orderedImages = regularLeftCop
                    updateImage(-1)
                }
            }

            Sprite {
                id: rightCop
                width: parent.width
                anchors.top: parent.top

                orderedImages:[
                    "",
                    "cop-b-00.svg",
                    "cop-b-01.svg",
                    "cop-b-02.svg",
                    "cop-b-03.svg",
                    "cop-b-04.svg",
                    "cop-b-04.svg",
                    "cop-b-03.svg",
                    "cop-b-02.svg",
                    "cop-b-01.svg",
                    "cop-b-00.svg",
                    ""
                ]

                onCurrentImageChanged: checkRightCop()

                Component.onCompleted: updateImage(-1)
            }

            Sprite {
                id: wall
                width: parent.width
                anchors.top: parent.top

                Connections {
                    target: root
                    onWallHoleChanged: wall.currentImage = wallHole
                }

                onCurrentImageChanged: showImage(wallHole)

                orderedImages: isGameB
                    ? [
                        "",
                        "hole-07.svg",
                        "hole-21.svg",
                        "hole-22.svg",
                        "hole-23.svg",
                        "hole-13.svg"
                    ]
                    : [
                        "",
                        "hole-11.svg",
                        "hole-12.svg",
                        "hole-13.svg"
                    ]
            }

            Sprite {
                id: player
                width: parent.width
                anchors.top: parent.top

                property var walkingPoints: []

                property var caughtByLightSprite: [
                    "",
                    "character-lightcaught-01.svg",
                    "",
                    "character-lightcaught-03.svg",
                    "character-lightcaught-04.svg",
                    "character-lightcaught-05.svg",
                    "",
                    "character-lightcaught-07.svg",
                    "",
                    "",
                    "",
                    ""
                ]

                property var regularSprite: [
                    "character-on-00.svg",
                    "character-on-01.svg",
                    "character-on-02.svg",
                    "character-on-03.svg",
                    "character-on-04.svg",
                    "character-on-05.svg",
                    "character-on-06.svg",
                    "character-on-07.svg",
                    "character-on-08.svg",
                    "character-on-09.svg",
                    "character-on-10.svg"
                ]

                Component.onCompleted: {
                    walkingPoints = [false,false,false,false,false,false,false,false,false,false]
                    orderedImages = regularSprite
                    showImage(0)
                }

                onCheckCollision: playerCaughtByLight()
            }

            Sprite {
                id: lost
                width: parent.width
                anchors.top: parent.top

                orderedImages:[
                    "character-lose-01.svg",
                    "empty.svg",
                    "character-lose-01.svg",
                    "empty.svg",
                    "character-lose-01.svg",
                    "empty.svg"
                ]
            }

            Sprite {
                property int livesLost: 0

                id: lives
                width: parent.width
                anchors.top: parent.top

                orderedImages:[
                    "empty.svg",
                    "character-lose-02.svg",
                    "character-lose-03.svg",
                    "character-lose-04.svg"
                ]

                onLivesLostChanged: {
                    showImage(livesLost)
                    if (livesLost == 3) {
                        console.log("Lost game")
                    }
                }
            }
        }

        Component.onCompleted: controls.forceActiveFocus()

        FocusScope {
            id: controls
            height: parent.height
            width: parent.width

            Keys.onLeftPressed: leftButton.clicked()
            Keys.onRightPressed: rightButton.clicked()

            focus: isGameRunning

            GameButton {
                id: leftButton
                anchors {
                    left: parent.left
                    leftMargin: buttonSideMargin
                }

                onClicked: if (!buttonPressed && isGameRunning && playerDelayed) {
                    buttonPressed = true
                    player.moveToLeft()
                }

                onReleased: buttonPressed = false
            }

            GameButton {
                id: rightButton
                anchors {
                    right: parent.right
                    rightMargin: buttonSideMargin
                }

                onClicked: if (!buttonPressed && isGameRunning && playerDelayed) {
                    buttonPressed = true
                    player.moveToRight()
                }

                onReleased: buttonPressed = false
            }
        }

        Rectangle {
            id: showScore
            anchors.centerIn: parent
            visible: false
            color: "#ee333333"
            width: parent.width
            height: parent.height /3.5

            Column {
                visible: parent.visible
                width: parent.width / 2

                anchors.centerIn: parent

                spacing: units.gu(2)

                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Game over")
                }

                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Score %1").arg(score)
                }

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Restart the game")

                    onClicked: {
                        lcd.opacity = 1
                        showScore.visible = false
                        lives.livesLost = 0
                        score = 0
                        lightSpeedTimer = 550
                        copLeftSpeedTimer = 1250
                        copRightSpeedTimer = 1150
                        levelUpFactor = 3
                        resetGame()
                    }
                }
            }
        }
    }

    function getRandom(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    function levelUp() {
        if (lightSpeedTimer > levelUpFactor) lightSpeedTimer -= levelUpFactor
        if (copLeftSpeedTimer > levelUpFactor) copLeftSpeedTimer -= levelUpFactor
        if (copRightSpeedTimer > levelUpFactor) copRightSpeedTimer -= levelUpFactor
        levelUpFactor += levelUpFactor * 0.3
    }

    function checkRightCop() {
        if ((rightCop.currentImage == 5 || rightCop.currentImage == 6) && player.currentImage !== 0) {
            isGameRunning = false
            loseAnimation()
        }
    }

    function checkLeftCop() {
        if ((leftCop.currentImage == 5) && player.currentImage == 8) {
            isGameRunning = false
            leftCop.orderedImages = leftCop.leftCopDog
            leftCop.showImage(0)
            loseAnimation()
        }
    }

    function playerCaughtByLight() {
        /* Light spots are from rigt to left: 0,1,2,3,4,5,6
         * [] are ordered by sprite number:
         * lightSpot[9] and lightSpot[1] are the ones with the barrel
         * playerSpot[0] is initial position,
         * playerSpot[2] is the barrel spot
         */

        var  lightSpot = [0,1,2,3,4,5,4,3,2,1]
        var playerSpot = [-1,0,-1,2,3,4,-1,5,-1,-1,-1]

        if (lightSpot[lightsOn.currentImage] == playerSpot[player.currentImage]) {
            isGameRunning = false
            player.orderedImages = player.caughtByLightSprite
            player.showImage(player.currentImage)
            loseAnimation()
        }
    }

    function resetPlayer() {
        player.orderedImages = player.regularSprite
        player.currentImage = 0
        player.showImage(player.currentImage)
        player.walkingPoints = [false,false,false,false,false,false,false,false,false,false]
        wall.empty()
        wallHole = 0
        playerDelayed = false
        isGameRunning = true
    }

    function resetGame() {
        if (lives.livesLost == 3) {
            //End game
            lcd.opacity = .4
            showScore.visible = true
        } else {
            lightsOn.currentImage = -1
            leftCop.orderedImages = leftCop.regularLeftCop
            leftCop.currentImage = -1
            rightCop.currentImage = -1
            resetPlayer()
        }
    }

    function loseAnimation() {
        animation.running = true
    }

    Timer {
        id: animation
        interval: 500
        running: false
        repeat: false

        onTriggered: {
            player.empty()
            wall.empty()
            lightsOn.empty()
            rightCop.empty()
            leftCop.empty()
            flick.running = true
        }
    }

    Timer {
        id: flick
        interval: 400
        running: false
        repeat: true

        onTriggered: {
            lost.currentImage = lost.currentImage + 1
            lost.showImage(lost.currentImage)

            if (lost.currentImage > lost.orderedImages.length) {
                lost.currentImage = 0
                flick.running = false
                lives.livesLost = lives.livesLost +1

                resetGame()
            }
        }
    }

    Timer {
        id: lightSpeed
        interval: lightSpeedTimer
        running: isGameRunning
        repeat: true

        onTriggered: {
            lightsOn.updateImage()
            playerDelayed = true
        }
    }

    Timer {
        id: copLeftSpeed
        interval: copLeftSpeedTimer
        running: isGameRunning
        repeat: true
        onTriggered: leftCop.updateImage()
    }

    Timer {
        id: copRightSpeed
        interval: copRightSpeedTimer
        running: isGameRunning
        repeat: true
        onTriggered: rightCop.updateImage()
    }
}
