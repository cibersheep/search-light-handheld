import QtQuick 2.4
import Lomiri.Components 1.3

Item {
    signal checkCollision

    property var orderedImages
    property int currentImage: 0
    property string imageUrl: "../../assets/lcd/"

    GameElement {
        id: _sprite
    }

    //For automatic movements
    function updateImage(startImage){
        if (startImage) {
            currentImage = startImage
        } else if (!currentImage && currentImage < -1) {
                currentImage = -1
        }

        currentImage = (currentImage + 1) % orderedImages.length

        if (orderedImages[currentImage]) {
            _sprite.image = imageUrl + orderedImages[currentImage]
        } else {
            _sprite.image = ""
        }

        checkCollision()

        if (debugging)
            console.log("updateIMG",startImage,currentImage,orderedImages[currentImage])
    }

    //For userInteraction. First sprite
    function showImage(image) {
        if (!image || image < 0)
            image = 0

        currentImage = image

        if (orderedImages[currentImage] && orderedImages[currentImage] !== "") {
            _sprite.image = imageUrl + orderedImages[currentImage]
        }
    }

    function empty() {
        _sprite.image = imageUrl + "empty.svg"
    }

    //For user interaction
    function moveToLeft() {
        //Check if beside the wall (before adding +1)
        if (currentImage == 8 || currentImage == 9) {
            if (wallHole > wall.orderedImages.length -2) {
                currentImage = 10
                root.score = root.score + 5
                //show empty wall sprite wall.showImage(0)
                isGameRunning = false
                _sprite.image = imageUrl + orderedImages[currentImage]
                roundEnd.running =true
                return
            } else {
                currentImage = 8
                wallHole = wallHole +1
                root.score = root.score + 1
                animation.running = true
            }
        } else {
            if (!player.walkingPoints[currentImage]) {
                player.walkingPoints[currentImage] = true
                root.score = root.score + 1
            }
        }

        currentImage = (currentImage + 1) % orderedImages.length
        checkCollision()
        checkLeftCop()
        checkRightCop()
        _sprite.image = imageUrl + orderedImages[currentImage]

        if (debugging)
            console.log("--player",currentImage,orderedImages[currentImage])
    }

    function moveToRight() {
        currentImage = (currentImage - 1) % orderedImages.length

        if (currentImage < 0) {
            currentImage = 0
            return
        }

        checkCollision()
        _sprite.image = imageUrl + orderedImages[currentImage]

        if (debugging)
            console.log("--player",currentImage,orderedImages[currentImage])
    }

    Timer {
        id: animation
        interval: 125
        running: false
        repeat: false

        onTriggered: {
            currentImage = 8
            _sprite.image = imageUrl + orderedImages[currentImage]
        }
    }

    Timer {
        id: roundEnd
        interval: 500
        running: false
        repeat: false

        onTriggered: resetPlayer()
    }
}
