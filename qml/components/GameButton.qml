import QtQuick 2.4
import Lomiri.Components 1.3

Item {
    signal clicked
    signal released

    //Delay rebutton action
    onClicked: rebutton.running = true


    property bool buttonPressed: false

    width: buttonWidth
    height: width/2.5

    anchors {
        bottom: parent.bottom
        bottomMargin: buttonBottomMargin
    }

    Rectangle {
        anchors.fill: parent
        color: "#000"
        radius: units.gu(1)
    }

    Button {
        width: parent.width - units.gu(2)
        height: width/3
        color: "#FFAE40"

        anchors {
            centerIn: parent
            bottom: parent.bottom
            bottomMargin: buttonBottomMargin
        }

        onClicked: parent.clicked()
    }

    Timer {
        id: rebutton
        interval: 80
        running: false
        repeat: false
        onTriggered: released()
    }
}
