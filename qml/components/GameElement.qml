import QtQuick 2.4
import Lomiri.Components 1.3

Image {
    id: _image
    property alias image: _image.source

    width: parent.width
    height: width * 3/4
    sourceSize.width: {
        if (lcd.width < units.gu(35)) {
            return units.gu(35)
        } else {
            return units.gu(60)
        }
    }
}
